package com.kcbassesment.kcbassessment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KcbassessmentApplication {

    public static void main(String[] args) {
        SpringApplication.run(KcbassessmentApplication.class, args);
    }

}
