/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kcbassesment.kcbassessment.arraysmanagement;

/**
 *
 * @author Simon.waweru
 */
/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
 */
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author Simon.waweru
 */
public class ArraysManagement {

    public static void main(String[] args) {
        ArraysManagement ar = new ArraysManagement();
        ar.execute();
    }

    public void execute() {
        int[] array = {1, 12, 7, 3, 6, 9, 7, 15, 1, 8, 4, 3, 9, 4, 5};

        Integer[] n = removeDuplicate(array);

        int[] newArray = new int[n.length];

        int x = 0;
        for (Integer in : n) {
            newArray[x] = in;
//System.out.println("Value " + newArray[x]);
            x++;
        }

        sortAnArray(newArray);

//********************Output*****************
        output(newArray);
    }

    public static Integer[] removeDuplicate(int[] arr) {
        Set<Integer> arrset = new HashSet<>();
        for (int i = 0; i < arr.length; i++) {
            arrset.add(arr[i]);
        }
        return arrset.toArray(new Integer[arrset.size()]);
    }

    private void sortAnArray(int[] arr) {
        System.out.println("Array elements after sorting:");
//sorting logic
        for (int i = 0; i < arr.length; i++) {
            for (int j = i + 1; j < arr.length; j++) {
                int tmp = 0;
                if (arr[i] > arr[j]) {
                    tmp = arr[i];
                    arr[i] = arr[j];
                    arr[j] = tmp;
                }
            }
        }
    }

//prints the sorted element of the array
    public void output(int[] arr) {
        for (int j = 0; j < arr.length; j++) {
            System.out.print(arr[j] + ",");
        }
    }

}
