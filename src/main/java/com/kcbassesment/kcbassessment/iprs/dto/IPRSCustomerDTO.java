/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kcbassesment.kcbassessment.iprs.dto;

import com.kcbassesment.kcbassessment.iprs.domain.IPRSCustomer;
import lombok.Data;

/**
 *
 * @author Simon.waweru
 */
@Data
public class IPRSCustomerDTO {

    private String firstName;
    private String middleName;
    private String lastName;
    private String gender;
    private String dob;
    private String idNumber;

    public static IPRSCustomer map(IPRSCustomerDTO data) {
        IPRSCustomer customer = new IPRSCustomer();
        customer.setDob(data.getDob());
        customer.setFirstName(data.getFirstName());
        customer.setGender(data.getGender());
        customer.setIdNumber(data.getIdNumber());
        customer.setLastName(data.getLastName());

        return customer;

    }

    public static IPRSCustomerDTO map(IPRSCustomer data) {
        IPRSCustomerDTO customerData = new IPRSCustomerDTO();
        customerData.setDob(data.getDob());
        customerData.setFirstName(data.getFirstName());
        customerData.setGender(data.getGender());
        customerData.setIdNumber(data.getIdNumber());
        customerData.setLastName(data.getLastName());
        return customerData;
    }
}
