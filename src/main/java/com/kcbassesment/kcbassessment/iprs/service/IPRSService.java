/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kcbassesment.kcbassessment.iprs.service;

import com.kcbassesment.kcbassessment.iprs.domain.IPRSCustomer;
import com.kcbassesment.kcbassessment.iprs.domain.IPRSCustomerRepository;
import com.kcbassesment.kcbassessment.iprs.dto.IPRSCustomerDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Simon.waweru
 */
@Service
public class IPRSService {

    @Autowired
    private IPRSCustomerRepository iPRSCustomerRepository;

    public IPRSCustomer saveIPRSCustomer(IPRSCustomerDTO data) {

        return iPRSCustomerRepository.save(data.map(data));
    }
}
