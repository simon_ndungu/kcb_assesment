/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kcbassesment.kcbassessment.iprs.domain;

import com.kcbassesment.kcbassessment.security.domain.Auditable;
import javax.persistence.Entity;
import lombok.Data;

/**
 *
 * @author Simon.waweru
 */
@Data
@Entity
public class IPRSCustomer extends Auditable {

    private String firstName;
    private String middleName;
    private String lastName;
    private String gender;
    private String dob;
    private String idNumber;
}
