/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kcbassesment.kcbassessment.iprs.domain;

import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author Simon.waweru
 */
public interface IPRSCustomerRepository extends JpaRepository<IPRSCustomer, Long> {

    Optional<IPRSCustomer> findByIdNumber(final String idNumber);
}
