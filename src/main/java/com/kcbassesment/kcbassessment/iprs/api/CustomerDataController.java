/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kcbassesment.kcbassessment.iprs.api;

import com.kcbassesment.kcbassessment.iprs.dto.IPRSCustomerDTO;
import com.kcbassesment.kcbassessment.iprs.service.IPRSService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
//import javax.validation.Valid;

/**
 *
 * @author Simon.waweru
 */
@RequiredArgsConstructor
@RestController
@RequestMapping("/api")
public class CustomerDataController {

    @Autowired
    private IPRSService iPRSService;

    @GetMapping("/iprs-customer/{idNumber}")
    public ResponseEntity<?> fetchIprsCustomerByIdNumber() {

        return ResponseEntity.ok("Successfully fetched");
    }

    @GetMapping("/iprs-customer")
    public ResponseEntity<?> fetchIprsCustomer() {
        System.out.println("We are doing fine");
        return ResponseEntity.ok("Successfully fetched");
    }

    @PostMapping("/iprs-customer")
    public ResponseEntity<?> addNewIPRSCustomer(@RequestBody IPRSCustomerDTO data) {

        return ResponseEntity.status(HttpStatus.CREATED).body(IPRSCustomerDTO.map(iPRSService.saveIPRSCustomer(data)));
    }
}
